<?php
class highlightjs {

    public function common($params, $hook) {
        if ($hook == "after_merge_css") {
            $params["/plugins/highlightjs/default.min.css"] = ["priority" => 200, "merge" => 0];
        }
        if ($hook == "after_merge_js") {
            $params["/plugins/highlightjs/highlight.min.js"] = ["priority" => 200, "merge" => 0];
            $params["/plugins/highlightjs/highlight.user.js"] = ["priority" => 200, "merge" => 1];
        }
        return $params;
    }
}
